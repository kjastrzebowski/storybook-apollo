FROM node:12.16-alpine3.9
LABEL maintainer="kjastrzebowski@auvik.com"

ARG NPM_TOKEN
ENV NPM_TOKEN=$NPM_TOKEN

EXPOSE 9009

WORKDIR /usr/src/app

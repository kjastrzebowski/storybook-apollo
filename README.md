# Storybook Apollo example

## Contributing

1. After pulling the repository please build the docker and install needed packages`:

```
docker-compose build storybook
docker-compose run --rm storybook yarn install
```

2. When the container is ready you can run it using `up` command:

```
docker-compose up
```

3. Finally the application is running on port 9009 on your local host: http://localhost:9009.


## Commands

### Install

Install dependencies using yarn:

```
docker-compose run --rm storybook yarn install
```

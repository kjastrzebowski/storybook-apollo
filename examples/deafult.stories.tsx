import React from "react";

import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

import ExchangeRates from "../src/ExchangeRates";

export default {
  title: "Components",
  decorators: [
    (story): JSX.Element => <div style={{ padding: "3rem" }}>{ story() }</div>,
  ],
  excludeStories: /.*Data$/,
};

const clientData = new ApolloClient({
  uri: `https://48p1r2roz4.sse.codesandbox.io`
});

export function Default(): JSX.Element {

  return (
    <ApolloProvider client={clientData}>
      <span>ExchangeRates:</span>
      <ExchangeRates />
    </ApolloProvider>
  );
}
